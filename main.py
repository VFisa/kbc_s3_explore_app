"__authors__ = 'Martin Fiser, Leo Chan'"
"__credits__ = 'Keboola 2017, Twitter: @VFisa'"

"""
Python 3 environment (unicode script fixes in place)
Boto docs: http://boto.cloudhackers.com
"""


import os
import io
import sys
import requests
import logging
import json
import gzip
from pygelf import GelfTcpHandler
import csv
import time
from keboola import docker
import boto
from boto.s3.key import Key
from boto.exception import S3ResponseError
import datetime
import dateparser
import pandas as pd


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)


#logging.basicConfig(level=logging.INFO,format='%(asctime)s - %(levelname)s - %(message)s',datefmt="%Y-%m-%d %H:%M:%S")

# initialize application
cfg = docker.Config('/data/')

# Access the supplied parameters
params = cfg.get_parameters()
KEY_ID = cfg.get_parameters()["keyId"]
SKEY_ID = cfg.get_parameters()["#secret_key"]
BUCKET = cfg.get_parameters()["bucket_source"]
ABBREV = cfg.get_parameters()["area"] #folder_string
FILE_NAME = cfg.get_parameters()["file_name"] #destination
#RANGE = cfg.get_parameters()["date_range"]
DATE_RANGE = cfg.get_parameters()["date_range"]
DEBUG = cfg.get_parameters()["debug"]
# in production, put this into parameter window:
"""
NEW config
{
    "keyId": "",
    "#secret_key": "",
    "bucket_source": "",
    "area": "",
    "file_name": "", 
    "start_date":"",
    "end_date":"",
    "debug": false
}
"""

# Logging
logger = logging.getLogger()
fields = {"_some": {"structured": "data"}}
if DEBUG==True:
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
        )
    """
    logger.addHandler(GelfTcpHandler(
        host=os.getenv('KBC_LOGGER_ADDR'),
        port=os.getenv('KBC_LOGGER_PORT'),
        debug=True, **fields
        ))
    """
    
else:
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
        )
    """
    logger.addHandler(GelfTcpHandler(
        host=os.getenv('KBC_LOGGER_ADDR'),
        port=os.getenv('KBC_LOGGER_PORT'),
        debug=False, **fields
        ))
    """


class sfile:
    """
    represents files on S3
    """

    def __init__(self, name, size, modified, key):
        self.name = name
        self.size = size
        self.modified = modified
        self.key = key


def list_items(bucket_name, folder=""):
    """
    list items in the bucket.
    returns a list of objects
    
    supported info:
        .name
        .key
        .size
        .last_modified
    
    if nonexistent is None:
        print "No such bucket!"
   

    # test if bucket is available
    buckets_available = list_buckets()
    if bucket_name in buckets_available:
        logging.info("Bucket found on S3: "+bucket_name)
    else:
        logging.error("Bucket is not present on S3. Exit!")
        sys.exit(1)
    """

    bucket = conn.get_bucket(bucket_name, validate=False)
    items = []

    for i in bucket.list(prefix=folder):
        name = i.name
        location = i.key
        size = i.size
        modified = i.last_modified

        print(str(name)+"|"+str(modified))

        info = sfile(name, size, modified, location)
        items.append(info)

    return items


def list_buckets():
    """
    List buckets and their properties
    """

    try:
        buckets = conn.get_all_buckets()
    except (OSError,S3ResponseError) as e:
        logging.error("Could not list buckets. Please check credentials. Exit!")
        sys.exit(0)

    buckets_names = []

    for i in buckets:
        name = i.name
        buckets_names.append(name)

    return buckets_names



if __name__ == "__main__":
    """
    Main execution script.
    """


    # Try out the connection
    try:
        conn = boto.connect_s3(KEY_ID,SKEY_ID)
        logging.info("Successfully connected."+str(conn))
    except Exception as a:
        logging.error("Could not connect. Exit!")
        sys.exit(1)

    # Basic information
    date_range = DATE_RANGE
    bucket_name = BUCKET
    folder_string = ABBREV
    destination = FILE_NAME
    
    #list_buckets()

    files = list_items(bucket_name)
    
    logging.info("Script completed.")
